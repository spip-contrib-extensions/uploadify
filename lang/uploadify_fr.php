<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'actuellement_lie' => 'Fichier(s) actuellement li&eacute;(s)',
	
	// F
	'fichiers_joints' => 'Fichiers joints',
	
	// O
	'option_groupe_description' => 'Upload de documents',
	
	// P
	'parcourir' => 'Parcourir...',
	
	// S
	'saisie_upload_titre' => 'Upload de documents',
	'saisie_upload_explication' => 'Ajoute un bouton au formulaire permettant d\'uploader des documents',
	
	// T
	"televerser" => "T&eacute;l&eacute;verser en lot ou t&eacute;l&eacute;verser de gros fichiers"
	
);

